#lang racket/base

(provide define-vat-run
         define-actormap-run)

(require "../core.rkt"
         syntax/parse/define
         (for-syntax racket/base
                     syntax/parse))

(define-simple-macro (define-vat-run id vat)
  (define-syntax (id vat-run-stx)
    (define body-stx (cdr (syntax-e vat-run-stx)))
    #`(vat 'run
            (lambda ()
              #,@body-stx))))

(define-syntax-rule (define-actormap-run id actormap)
  (define-syntax (id am-run-stx)
    (define body-stx (cdr (syntax-e am-run-stx)))
    #`(actormap-run! actormap
                     (lambda ()
                       #,@body-stx))))

(module+ test
  (require rackunit
           "../vat.rkt")
  (define a-vat (make-vat))
  (define-vat-run a-vat-run
    a-vat)
  (check-equal?
   (a-vat-run
    (define a-frond
      (spawn (lambda (bcom) (lambda () 'hello-frond))))
    ($ a-frond))
   'hello-frond)

  (define am (make-actormap))
  (define-actormap-run am-run am)
  
  (check-equal?
   (am-run
    (define a-frog
      (spawn (lambda (bcom) (lambda () 'hello-frog))))
    ($ a-frog))
   'hello-frog))
