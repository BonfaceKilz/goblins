#lang racket/base

(provide ^hash ^hasheq ^hasheqv
         ^filo-queue
         ^set ^seteq ^seteqv)

(require "../core.rkt"
         "methods.rkt"
         racket/set)

(define (^hash bcom [ht #hash()])
  (methods
   [ref
    (case-lambda
      [(key)
       (hash-ref ht key)]
      [(key dflt)
       (hash-ref ht key dflt)])]
   [(set key val)
    (bcom (^hash bcom (hash-set ht key val)))]
   [(has-key? key)
    (hash-has-key? ht key)]
   [(keys) (hash-keys ht)]
   [(values) (hash-values ht)]
   [(data) ht]))

;; Really just sets up a new hasheq
;; Doesn't check it but maybe it should.
(define (^hasheq bcom)
  (^hash bcom #hasheq()))

(define (^hasheqv bcom )
  (^hash bcom #hasheqv()))

(define (^filo-queue bcom [lst '()])
  (methods
   [(empty?)
    (null? lst)]
   [(push item)
    (bcom (^filo-queue bcom (cons item lst)))]
   [(pop)
    (when (null? lst)
      (error "Tried to pop empty queue"))
    (bcom (^filo-queue bcom (cdr lst))
          (car lst))]
   [(append new-lst)
    (bcom (^filo-queue bcom (append new-lst lst)))]
   [(data) lst]))

(define (^set bcom [s (set)])
  (methods
   [(add val)
    (bcom (^set bcom (set-add s val)))]
   [(remove val)
    (bcom (^set bcom (set-remove s val)))]
   [(member? val)
    (set-member? s val)]
   [(data) s]))

(define (^seteq bcom)
  (^set bcom (seteq)))

(define (^seteqv bcom)
  (^set bcom (seteqv)))

(module+ test
  (require rackunit)

  (define am (make-actormap))

  (define ht
    (actormap-spawn! am ^hasheq))

  (actormap-poke! am ht 'set 'foo 1)
  (actormap-poke! am ht 'set 'bar 2)
  
  (check-equal? (actormap-peek am ht 'data)
                #hasheq((foo . 1)
                        (bar . 2)))

  (define s
    (actormap-spawn! am ^seteq))

  (actormap-poke! am s 'add 'foo)
  (actormap-poke! am s 'add 'bar)
  (check-equal? (actormap-peek am s 'data) (seteq 'foo 'bar))

  (define q
    (actormap-spawn! am ^filo-queue))
  (actormap-poke! am q 'push 'beep)
  (actormap-poke! am q 'push 'boop)
  (check-equal? (actormap-peek am q 'data) '(boop beep))
  (check-equal? (actormap-poke! am q 'pop) 'boop)
  (check-equal? (actormap-peek am q 'data) '(beep))

  )
